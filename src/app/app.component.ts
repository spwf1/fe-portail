import {AfterViewChecked, Component, OnInit, ViewChild} from '@angular/core';
import {AbstractFederated, TransversalSubMenuService} from "fe-persee-commun";
import {
  MainMenuEntry,
  TransversalMenuMicroFrontendConfig,
  TransversalMenuModel
} from "./components/transversal-menu/transversal-menu.model";
import {TransversalMenuService} from "./components/transversal-menu/transversal-menu.service";
import {TransversalSubMenuComponent} from "./components/transversal-sub-menu/transversal-sub-menu.component";
import {TransversalMenuComponent} from "./components/transversal-menu/transversal-menu.component";
import {NavigationEnd, Router} from "@angular/router";
import {filter} from "rxjs";
import {environment} from "../environment/environment";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent extends AbstractFederated implements OnInit, AfterViewChecked {
  title = 'fe-persee-portail';

  @ViewChild("subMenu")
  subMenu: TransversalSubMenuComponent;
  @ViewChild("mainMenu")
  mainMenu: TransversalMenuComponent;

  menuItems: TransversalMenuModel[] = [];
  remoteMicroFrontendUrl: string[] = [];

  //Define main menu entries in portal, sub will be added by fetching all micro frontend config
  mainMenuKeys: { mainMenuEntry: MainMenuEntry, icon: string, routerLink?: string }[] = [
    {mainMenuEntry: MainMenuEntry.CREANCE, icon: "fa-solid fa-scale-balanced"},
    {mainMenuEntry: MainMenuEntry.DETTES, icon: "fa-solid fa-book"}
  ];

  constructor(private menuTransversalService: TransversalMenuService,
              private transversalSubMenuService: TransversalSubMenuService,
              private router: Router,
              private translateService: TranslateService) {
    super();
    this.translateService.use('en');
    const appNames = Object.keys(environment.mfeUrls);
    appNames.forEach(app => this.remoteMicroFrontendUrl.push((environment.mfeUrls as any)[app]));
  }

  ngOnInit(): void {
    this.buildFirstLevelMenu();
    this.loadRemoteMenuConfigs();
  }

  ngAfterViewChecked(): void {
    // On check si le menu de niveau 3 doit rester ouvert ou ferme sur un changement de route
    // Pour cela on doit accéder au component actuel et check si il a une componentId ou parentId semblable a celle stocker dans le subMenu service
    // Le current component doit être fill dans le submenu service dans le constructeur du component concerné et passé a undefined dans le onDestroy
    if (this.subMenu.isDisplayed) {
      this.router.events
        .pipe(filter(event => event instanceof NavigationEnd))
        .subscribe((event: any) => {
          const currentComponent = this.transversalSubMenuService.currentComponentInstance;
          if (!currentComponent) {
            this.transversalSubMenuService.emitEvent(null);
            this.transversalSubMenuService.openedMenu = undefined;
          }
        });
    }
  }

  getOpenMenuClass(): string {
    if (this.subMenu?.isDisplayed || this.mainMenu?.isOpenMenu()) {
      return "main-container-opened-menu"
    } else {
      return "main-container";
    }
  }

  private buildFirstLevelMenu() {
    this.menuItems = this.mainMenuKeys.map(entry => ({
      icon: entry.icon,
      id: entry.mainMenuEntry,
      tooltip: entry.mainMenuEntry.toString(),
      label: entry.mainMenuEntry.toString(),
      routerUrl: entry.routerLink,
      items: []
    }));
  }

  private loadRemoteMenuConfigs() {
    this.remoteMicroFrontendUrl.forEach(url => this.loadRemoteModule(url, 'MenuEntry')
      .then((remoteConfig: TransversalMenuMicroFrontendConfig) => {
        this.menuItems.forEach(menuItem => {
          const mainMenuConfig = remoteConfig.availableEntryByParentKey.find(config => config.mainMenu === menuItem.id);
          mainMenuConfig?.availableMenuId.forEach(availableSubMenuKey => {
            const transversalMenuSubItem = remoteConfig.microFrontendMenuElement.find(microFrontend => microFrontend.id === availableSubMenuKey);
            if (transversalMenuSubItem) {
              menuItem.items?.push(transversalMenuSubItem);
            }
          });
        });
      }).catch(e => {
      })
    );
  }
}
