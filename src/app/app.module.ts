import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {AppRoutingModule} from "./app-routing.module";
import {FePerseeCommunModule} from "fe-persee-commun";
import {RouterModule} from "@angular/router";
import {APP_ROUTES} from "./app.routes";
import {PortailModule} from "./components/portail.module";
import {TransversalMenuComponent} from "./components/transversal-menu/transversal-menu.component";
import {
  TransversalMenuItemComponent
} from "./components/transversal-menu/transversal-menu-item/transversal-menu-item.component";
import {TransversalSubMenuComponent} from "./components/transversal-sub-menu/transversal-sub-menu.component";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TooltipModule} from "primeng/tooltip";
import {TranslateLoader, TranslateModule} from "@ngx-translate/core";
import {TranslateHttpLoader} from "@ngx-translate/http-loader";
import {HttpClient, HttpClientModule} from "@angular/common/http";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    TransversalMenuComponent,
    TransversalMenuItemComponent,
    TransversalSubMenuComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(APP_ROUTES),
    AppRoutingModule,
    FePerseeCommunModule,
    PortailModule,
    TooltipModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
