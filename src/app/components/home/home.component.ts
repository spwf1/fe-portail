import {Component, OnInit} from '@angular/core';
import {FederatedOutputEvent, SharedService} from "fe-persee-commun";
import {TranslateService} from "@ngx-translate/core";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  sharedData: string;

  public val = 0;

  constructor(private sharedService: SharedService,
              private translateService: TranslateService) {
    this.translateService.use("en");
    console.log(this.translateService.instant("HELLO"));
  }

  ngOnInit(): void {
  }

  saveSharedData() {
    this.sharedService.setData(this.sharedData);
  }

  manageFederatedOutput(event: FederatedOutputEvent<number>) {
    if (event.event === 'coucouEmit') {
      this.val = event.value;
    }
    console.log(event);
  }


}
