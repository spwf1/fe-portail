import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HomeComponent} from "./home/home.component";
import {PortailRoutingModule} from "./portail-routing.module";
import {SharedModule} from "../shared/shared.module";
import {FePerseeCommunModule} from "fe-persee-commun";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {TranslateModule} from "@ngx-translate/core";


@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    PortailRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule,
    FePerseeCommunModule,
    TranslateModule
  ]
})
export class PortailModule {
}
