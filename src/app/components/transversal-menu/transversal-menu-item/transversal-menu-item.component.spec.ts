import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransversalMenuItemComponent } from './transversal-menu-item.component';

describe('TransversalMenuItemComponent', () => {
  let component: TransversalMenuItemComponent;
  let fixture: ComponentFixture<TransversalMenuItemComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TransversalMenuItemComponent]
    });
    fixture = TestBed.createComponent(TransversalMenuItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
