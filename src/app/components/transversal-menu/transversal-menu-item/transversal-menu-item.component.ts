import {Component, EventEmitter, Input, Output} from '@angular/core';
import {TransversalMenuModel} from "../transversal-menu.model";
import {Router} from "@angular/router";

@Component({
  selector: 'por-transversal-menu-item',
  templateUrl: './transversal-menu-item.component.html',
  styleUrls: ['./transversal-menu-item.component.scss']
})
export class TransversalMenuItemComponent {

  @Input()
  menuItem: TransversalMenuModel = {};
  @Input()
  isMenuOpen = false;
  @Input()
  isSelected = false;
  @Input()
  isHighlighted = false;
  @Output()
  openTransversalMenu = new EventEmitter<TransversalMenuModel>();
  @Output()
  closeMenu = new EventEmitter();

  constructor(private router: Router) {
  }
  onOpenMenu() {
    this.openTransversalMenu.emit(this.menuItem);
  }

  isSelectedClass(): string {
    return this.isSelected ? 'selected' : '';
  }

  navigate(item: TransversalMenuModel) {
    this.router.navigateByUrl(item.routerUrl);
    this.closeMenu.emit();
  }
}
