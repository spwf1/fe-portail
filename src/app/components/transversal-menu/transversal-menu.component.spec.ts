import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransversalMenuComponent } from './transversal-menu.component';

describe('TransversalMenuComponent', () => {
  let component: TransversalMenuComponent;
  let fixture: ComponentFixture<TransversalMenuComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TransversalMenuComponent]
    });
    fixture = TestBed.createComponent(TransversalMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
