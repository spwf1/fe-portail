import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {MainMenuEntry, TransversalMenuModel} from "./transversal-menu.model";
import {NavigationEnd, Router} from "@angular/router";
import {TransversalMenuService} from "./transversal-menu.service";
import {filter} from "rxjs";

@Component({
  selector: 'por-transversal-menu',
  templateUrl: './transversal-menu.component.html',
  styleUrls: ['./transversal-menu.component.scss']
})
export class TransversalMenuComponent implements OnInit {

  @Input()
  menuItems: TransversalMenuModel[] = [];
  @Input()
  footerMenuItems: TransversalMenuModel[];
  @Input()
  homeRouting = "";
  openedItem: TransversalMenuModel;

  @ViewChild("menuRef")
  menuRef: ElementRef;

  constructor(private router: Router,
              private menuTransversalService: TransversalMenuService) {
  }

  ngOnInit(): void {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe((event: any) => {
        this.menuItems.forEach(menuItem => {
          if (this.isMenuItemContainUrl(menuItem, event.url)) {
            this.openedItem = menuItem;
          }
        })
      });
  }

  isMenuItemContainUrl(item: TransversalMenuModel, url: string): boolean {
    const urls: string[] = [];
    if (item.routerUrl) {
      urls.push(item.routerUrl);
    }
    if (item.items && item.items?.length > 0) {
      item.items.map(item => item.routerUrl)
        .forEach(url => urls.push(url));
    }
    return urls.includes(url);
  }

  openMenuOnElement(event: TransversalMenuModel, forceOpenSubMenu?: boolean) {
    this.openedItem = event;
    if (event.routerUrl && !forceOpenSubMenu) {
      this.router.navigateByUrl(event.routerUrl);
    } else {
      this.openMenu();
    }
  }

  goToHome() {
    this.router.navigateByUrl(this.homeRouting);
    this.closeMenu();
  }
  closeMenu() {
    this.menuTransversalService.isMainMenuOpen = false;
  }

  openMenu() {
    this.menuTransversalService.isMainMenuOpen = true;
  }

  isOpenMenu(): boolean {
    return this.menuTransversalService.isMainMenuOpen;
  }
}
