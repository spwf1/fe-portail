export class TransversalMenuModel {
  tooltip?: string;
  icon?: string;
  id?: MainMenuEntry;
  label?: string;
  routerUrl?: string;
  items?: TransversalMenuModel[];
}
export class TransversalMenuMicroFrontendConfig {
  availableEntryByParentKey?: AvailableMenuForMainMenu[];
  microFrontendMenuElement?: TransversalMenuModel[];
}
export enum MainMenuEntry {
  CREANCE = "CREANCE",
  DETTES = "DETTES"
}
export class AvailableMenuForMainMenu {
  mainMenu?: MainMenuEntry;
  availableMenuId?: string[];
}
