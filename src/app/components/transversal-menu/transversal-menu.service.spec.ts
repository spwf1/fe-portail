import { TestBed } from '@angular/core/testing';

import { TransversalMenuService } from './transversal-menu.service';

describe('TransversalMenuService', () => {
  let service: TransversalMenuService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TransversalMenuService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
