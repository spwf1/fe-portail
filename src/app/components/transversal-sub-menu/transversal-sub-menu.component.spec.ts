import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TransversalSubMenuComponent } from './transversal-sub-menu.component';

describe('TransversalSubMenuComponent', () => {
  let component: TransversalSubMenuComponent;
  let fixture: ComponentFixture<TransversalSubMenuComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TransversalSubMenuComponent]
    });
    fixture = TestBed.createComponent(TransversalSubMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
