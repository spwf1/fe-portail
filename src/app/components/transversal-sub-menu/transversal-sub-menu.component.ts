import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AbstractFederated, TransversalSubMenuService} from "fe-persee-commun";
import {environment} from "../../../environment/environment";
import {Router} from "@angular/router";

@Component({
  selector: 'por-transversal-sub-menu',
  templateUrl: './transversal-sub-menu.component.html',
  styleUrls: ['./transversal-sub-menu.component.scss']
})
export class TransversalSubMenuComponent extends AbstractFederated implements OnInit, AfterViewInit {

  isDisplayed = false;
  subMenuItems: TransversalSubMenuModel[];
  entityId: string;

  constructor(private transversalSubMenuService: TransversalSubMenuService,
              private router: Router) {
    super();
    this.transversalSubMenuService.getEvent().subscribe(event => {
      if (event) {
        transversalSubMenuService.openedMenu = event;
        this.loadMenuConfigForComponentId(event)
      } else {
        this.isDisplayed = false;
      }
    });
  }

  loadMenuConfigForComponentId(componentId: string) {
    const appNames = Object.keys(environment.mfeUrls);
    this.subMenuItems = [];
    appNames.forEach(key => {
      this.loadRemoteModule((environment.mfeUrls as any)[key], "SubMenuEntry")
        .then((config: TransversalSubMenuModel[]) => {
          const itemToAdd = config.filter(conf => conf.componentId === componentId);
          itemToAdd.forEach(item => {
            if (!this.subMenuItems.find(subItem => subItem.route === item.route)) {
              this.subMenuItems.push(item);
            }
          })
        }).catch(e => {
      });
    });
    this.isDisplayed = true;
  }

  ngOnInit(): void {
    this.transversalSubMenuService.getEntity()
      .subscribe(entityId => this.entityId = entityId);
  }

  ngAfterViewInit(): void {
    if (!this.isDisplayed && this.subMenuItems) {
      this.subMenuItems.forEach(item => {
        if (this.buildRoute(item) === this.getCurrentRoute()) {
          this.isDisplayed = true;
        }
      })
    }
  }

  buildRoute(item: TransversalSubMenuModel) {
    return item.route + '/' + this.entityId;
  }

  getSelectedClass(item: TransversalSubMenuModel): string {
    return this.buildRoute(item) === this.getCurrentRoute() ? 'selected' : '';
  }

  private getCurrentRoute(): string {
    return this.router.routerState.snapshot.url;
  }
}

export interface TransversalSubMenuModel {

  route: string,
  componentId: string

}
