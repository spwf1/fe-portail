export const environment = {
  production: false,
  mfeUrls: {
    creance: "http://localhost:4202/remoteEntry.js",
    dettes: "http://localhost:4203/remoteEntry.js",
    recouvrement: "http://localhost:4204/remoteEntry.js",
    redevable: "http://localhost:4205/remoteEntry.js",
    gestionTache: "http://localhost:4206/remoteEntry.js",
  }
};
