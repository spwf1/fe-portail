const ModuleFederationPlugin = require("webpack/lib/container/ModuleFederationPlugin");
const mf = require("@angular-architects/module-federation/webpack");
const path = require("path");
const share = mf.share;

const sharedMappings = new mf.SharedMappings();
sharedMappings.register(
  path.join(__dirname, 'tsconfig.json'),
  [/* mapped paths to share */]);

module.exports = {
  output: {
    uniqueName: "fePerseePortail",
    publicPath: "auto",
    scriptType: 'text/javascript'
  },
  optimization: {
    runtimeChunk: false
  },
  resolve: {
    alias: {
      ...sharedMappings.getAliases(),
    }
  },
  experiments: {
    outputModule: true
  },
  plugins: [
    new ModuleFederationPlugin({
        library: { type: "module" },

        // For remotes (please adjust)
        //name: "fePerseePortail",
        //filename: "remoteEntry.js",
        //exposes: {
         //    PortailHomeComponent: './/src/app/components/home/home.component.ts',
        //},

        // For hosts (please adjust)
         //remotes: {
          // "creanceConfig": "http://localhost:4202/remoteEntry.js",
        //},

        shared: share({
          "@angular/core": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
          "@angular/common": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
          "@angular/common/http": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
          "@angular/router": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
          "@angular/platform-browser/animations": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
          "@ngx-translate/core": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
          "@ngx-translate/http-loader": { singleton: false, strictVersion: true, requiredVersion: 'auto' },
          "fe-persee-commun": { singleton: false, strictVersion: false, requiredVersion: 'auto' },
          ...sharedMappings.getDescriptors()
        })

    }),
    sharedMappings.getPlugin()
  ],
};
